## Neovim playbook for installation on local machine

## Description
This playbook:
- Installs or updates neovim to ```$HOME/.local/neovim''' (unless otherwise specified in vars)
- Creates a desktop shortcut and icon
- If no init.lua is present, will create one.

